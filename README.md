# README #

## What is this repository for? ##

QtQuick2OSGItem is a small package allowing to embed OpenSceneGraph scenes as
visual items in Qt Quick 2 applications. The package provides a QML extension
plugin and a sample application.

## QML Extension Types ##

### OSGItem ###

`OSGItem` is a QML type derived from the QML `Item` type. It merely encapsulates
a pointer to an `osg::Node` (OSG model) object but doesn't expose it to QML in
any way. It is assumed that the model must be set from within C++.

> Once a model is bound to an `OSGItem` it must NOT be modified in any way in
> order to avoid thread-safety issues (rendering of the OSG scene will happen
> in a separate Qt thread).

`OSGItem` allows manipulating the scene view with the mouse.

### OSGModelLoader ###

The plugin also contains an auxiliary type `OSGModelLoader` intended for
applications where scenes must be loaded from files. `OSGModelLoader`  is used
in the demo application and serves as an example of how to interact with `OSGItem`
from C++ code (in those applications where the scene is created from C++ code).

## Limitations ##

* Currently `OSGItem` doesn't allow controlling from QML any aspect of the scene
(e.g. the camera location, background color, manipulation mode, etc).
* Dynamic OSG models are rendered statically (they are updated only as a result
of the user manipulating the scene view).
* Build process doesn't include an install step (or creation of a deployable package)


## Building ##

### Dependencies ###

* Qt 5.5 Quick and Core modules
* OpenSceneGraph osg, osgDB, osgGA, and osgViewer modules
* zlib (indirectly, through OSG)

The code should work at least with the following build environment:

* CMake 3.3.2
* Qt Creator 3.5.1 / Qt 5.5.1
* MSVC2013
* OpenSceneGraph 3.2.3

### Setting up under Qt Creator ###

> These instructions address the build process under Windows. In my own experience,
> development processes are much simpler under Linux (besides, if you work under
> Linux, you're less likely to be in need of these instructions).

1. Open the top level CMakeLists.txt file with Qt Creator (File -> Open File or Project...)
2. Select the location of the build directory
3. Select the generator/kit and push the "Run CMake" button. 
4. If CMake fails because it didn't detect the location of the OSG packages automatically,
then open the CMakeCache.txt file in the build directory, specify correct values for
the following CMake variables and rerun CMake:

    * OPENTHREADS_INCLUDE_DIR
    * OPENTHREADS_LIBRARY
    * OPENTHREADS_LIBRARY_DEBUG
    * OSGDB_INCLUDE_DIR
    * OSGDB_LIBRARY
    * OSGDB_LIBRARY_DEBUG
    * OSGGA_INCLUDE_DIR
    * OSGGA_LIBRARY
    * OSGGA_LIBRARY_DEBUG
    * OSGVIEWER_INCLUDE_DIR
    * OSGVIEWER_LIBRARY
    * OSGVIEWER_LIBRARY_DEBUG
    * OSG_INCLUDE_DIR
    * OSG_LIBRARY
    * OSG_LIBRARY_DEBUG

    > xxx_INCLUDE_DIR variables must point to the *parent* directory of the
    > directory containing the header files of the corresponding package.
    > For example, if the header files of the OpenThreads package are located
    > under "C:\Tools\OpenSceneGraph\include\OpenThreads" then OPENTHREADS_INCLUDE_DIR
    > must be set to C:\Tools\OpenSceneGraph\include. As a result, in my set-up all
    > xxx_INCLUDE_DIR variables have the same value (since OpenThreads is included
    > with the OpenSceneGraph package).

5. Build
6. Run. This will run the sample application. If the opened window is empty and
the following error is displayed in the "Application Output" pane

        plugin cannot be loaded for module "OSG": Cannot load library
        C:/YOUR/PATH/MAY/VARY/qtquick2osgitem/build/OSG/osg_plugin.dll:
        The specified module could not be found

    then most probably either OSG or zlib are not in your path. Add their paths to the
    PATH environment variable either system wide, or through the Qt Creator project
    settings (Projects -> Build & Run -> Run -> Run Environment).


## Credits ##

This project derives to some extent from Konstantin Podsvirov's
[osgqtquick][osgqtquick] project. The main differences are:

* QtQuick2OSGItem has a much narrower scope - its only goal is to allow *embedding*
OSG models in Qt Quick 2 applications, whereas [osgqtquick][osgqtquick]
also provides facilities for *building* OSG models from within QML.

* QtQuick2OSGItem takes advantage of frame-buffer support (`QQuickFramebufferObject`)
that was added to Qt Quick after the [osgqtquick][osgqtquick]
project was started. Besides, some other improvements/simplifications were made to
the code responsible for integrating the rendering of OSG in Qt Quick 2 environment.

[osgqtquick]: https://inqlude.org/libraries/osgqtquick.html